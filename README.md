**Linear and hash search**

This repository contains algorithms for linear and hash search string comparison.
It also contains a data generator and tools for testing, measuring and comparing the complexity of algorithms.
![alt text](https://gitlab.com/rav1ll/team_6/-/raw/master/load_testing_plots/1621693954.png "Описание будет тут") 
This package can be used as a command line tool. 


You can **download** this package on https://pypi.org/project/stringcompare/



**HOW TO USE:**

You can use this package as command line tool:

using these commands:

```
stringcompare chart --file PATH TO CSV TABLE

stringcompare generate --start --end --step --count

stringcompare measure-algo --func FUNC NAME

stringcompare process1 --data 2STRINGS

stringcompare process2 --data  2 STRINGS

```


**HOW TO START:**

You need at least Python 3.8 and pip installed to run this CLI tool.

1.Install it using pip
`pip3 install stringcompare`

2.Create virtualenv

3.Call `--help` to ensure that it is successfully installed
`stringcompare --help`

4.Run one of commands (examples given below)

```
stringcompare chart --file load_testing_measurements\1621540541.csv

stringcompare generate --start 1 --end 22 --step 5 --count 5

stringcompare measure-algo --func linear_search

stringcompare process1 --data ghahwe lsadas

stringcompare process2 --data python zebra
```

5. Profit !
